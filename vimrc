" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
"if has("autocmd")
"  filetype plugin indent on
"endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden		" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes) "enabled william doyle december 31st 2019 then promptly disabled again

set tabstop=4			"tab settings william doyle feb 4th 2020
set softtabstop=0 noexpandtab	"feb 4th 2020 william doyle
set shiftwidth=4		"feb 4th 2020 william doyle

setlocal foldmethod=syntax	"feb 4th 2020 william doyle 
set foldnestmax=2			"feb 4th 2020 william doyle

let javaScript_fold = 2		"feb 6th 2021 wdd --allow folding in javascript


colorscheme torte 


"january 21st 2021 wdd mapping 2 semicolons to Esc because Esc is far away
"from home row of keyboard
"imap ;; <Esc>				


" Install vim-plug if not found	" wdd January 17th 2021
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins	" wdd January 17th 2021
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin('~/.vim/plugged')	" wdd January 17th 2021
	" P U T    P L U G I N S    H E R E 
 Plug 'neoclide/coc.nvim', {'branch': 'release'} " wdd kinda makes everything feel laggy I'm not sure I want this
	Plug 'tomlion/vim-solidity'		" solidity syntax support
	"Plug 'tpope/vim-sensible'
"	Plug 'scrooloose/nerdtree'
	Plug 'tikhomirov/vim-glsl'	" wdd january 24th 2021 -> support for glsl syntax
"	Plug 'sheerun/vim-polyglot' " wdd feb 7th 2021 -- cool but way too much
    Plug 'pangloss/vim-javascript' " wdd feb 7th 2021 -- better support for javascript
	Plug 'maxmellon/vim-jsx-pretty'
	Plug 'dense-analysis/ale'	" linter march 29th 2021 -- this plugin is on probation
	Plug 'arnoudbuzing/wolfram-vim'
	Plug 'purescript-contrib/purescript-vim' " sept 19th 2021
call plug#end()

" In ~/.vim/vimrc, or somewhere similar.
let g:ale_linters = {
\   'javascript': ['prettier','eslint'],
\	'haskell': ['hlint']
\}

"let g:ale_linters_explicit = 1
let g:ale_fixers = {
\	'javascript':['prettier', 'eslint'],
\	'haskell': ['hlint']
\}

let g:ale_fix_on_save = 1
let g:ale_lint_on_save = 1
let g:ale_javascript_prettier_options = '--single-quote --trailing-comma all'
let g:ale_javascript_eslint_options = '--single-quote --trailing-comma all'
let g:ale_enabled = 1

let g:ale_haskell_hlint_executable = 'hlint'
let g:ale_haskell_hlint_options = ''


" use <tab> for trigger completion and navigate to the next complete item
"function! s:check_back_space() abort
"  let col = col('.') - 1
"  return !col || getline('.')[col - 1]  =~ '\s'
"endfunction

"inoremap <silent><expr> <Tab>
"      \ pumvisible() ? "\<C-n>" :
"      \ <SID>check_back_space() ? "\<Tab>" :
"      \ coc#refresh()



" Declare CoC extensions
"let g:coc_global_extensions = [
"  \ 'coc-tsserver'
"  \ ]



" --------------------------------------------------------- wdd feb 8th 2021 --  concealing ... kinda cool
"let g:javascript_conceal_function             = "ƒ"
"let g:javascript_conceal_arrow_function       = "⇒"
"set conceallevel=1
"let g:vim_json_syntax_conceal = 0

"----------------------------------------------------------
let mapleader = ","					" wdd January 17th 2021
nmap <leader>ne :NERDTreeToggle<cr>

"set number "  william doyle November/21st/2019
set number relativenumber 	" william doyle december/8th/2019
set cursorline 				" William Doyle December 15th 2019
set cursorcolumn 			" William Doyle December 15th 2019
set clipboard=unnamedplus	" use system clipboard wdd january 10th 2021
" _________________________________________________________________________________________________
" Highlight all instances of word under cursor, when idle.
" Useful when studying strange source code.
" Type z/ to toggle highlighting on/off.
" william doyle febuary 6th 2020 SOURCE: vim.fandom.com/wiki/Auto_highlight_current_word_when_idle
set hls
nnoremap z/ :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>
function! AutoHighlightToggle()
  let @/ = ''
  if exists('#auto_highlight')
    au! auto_highlight
    augroup! auto_highlight
    setl updatetime=4000
    echo 'Highlight current word: off'
    return 0
  else
    augroup auto_highlight
      au!
      au CursorHold * let @/ = '\V\<'.escape(expand('<cword>'), '\').'\>'
    augroup end
    setl updatetime=500
    echo 'Highlight current word: ON'
    return 1
  endif
endfunction

" _________________________________________________________________________________________________
"-------------------------keep centered -- https://vim.fandom.com/wiki/Keep_your_cursor_centered_vertically_on_the_screen
"wdd feb 10 2021
augroup VCenterCursor
  au!
  au BufEnter,WinEnter,WinNew,VimResized *,*.*
        \ let &scrolloff=winheight(win_getid())/2
augroup END
"------------------------------------------------------end keep centered
" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

